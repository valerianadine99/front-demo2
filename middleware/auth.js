/*
Previene ingresar a páginas cuando el usuario aún no se ha autenticado
Se carga después del NuxtServerInit y antes de renderizar cualquier página
*/
export default function ({ redirect, store }) {
  if (!store.state.authUser) {
    console.log("No ha ingresado, regrese al login");
    return redirect('/')
    }
  }
